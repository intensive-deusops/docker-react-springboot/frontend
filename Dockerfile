FROM node:16-alpine AS builder

ARG BK_HOST=localhost
ARG BK_PORT=8080

COPY src /app/src
COPY public /app/public
COPY package* /app

WORKDIR /app

RUN set -eux; \
    sed -i  "s|localhost|${BK_HOST}|g" /app/src/components/Student.js; \
    sed -i  "s|8080|${BK_PORT}|g" /app/src/components/Student.js; \
    npm install; \
    npm run build

FROM node:16-alpine
COPY entrypoint /usr/bin
COPY --from=builder /app /app
RUN chmod +x /usr/bin/entrypoint
WORKDIR /app
ENTRYPOINT [ "entrypoint" ]
#CMD ["npm", "start"]
